# Awesome Me!

A place to centralize my favorite web resources.

The source is [mdBook] content, deployed through Gitlab Pages to https://epmills.gitlab.io/awesome-me/.


[mdBook]: https://github.com/rust-lang/mdBook