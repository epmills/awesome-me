# Decentralization

*Blockchain-based and other decentralized networks and services*

- [disroot](https://disroot.org/en)<br/>
  Disroot is a platform providing online services based on principles of
  freedom, privacy, federation and decentralization

- [MaidSafe](https://maidsafe.net)<br/>
  The World's First Autonomous Data Network

- [NKN](https://www.nkn.org)<br/>
  Highly scalable consensus, proof of relay and tokenized connectivity

- [Radicle](http://www.radicle.xyz)<br/>
  A peer-to-peer stack for code collaboration