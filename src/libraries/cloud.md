## Cloud

*Libraries and frameworks for cloud native applications*

- [pulumi.io](https://pulumi.io)<br/>
  A multi-language, multi-cloud development platform

- [google/go-cloud](https://github.com/google/go-cloud)<br/>
  A library and tools for open cloud development in Go
