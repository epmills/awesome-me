## Go

*Libraries and frameworks for the go language*

- [droyo/styx](https://github.com/droyo/styx)<br/>
  Go library for the 9P filesystem protocol

- [matryer/anno](https://github.com/matryer/anno)<br/>
  Go package for text annotation.

- [matryer/vice](https://github.com/matryer/vice)<br/>
  Go channels at horizontal scale (powered by message queues)

- [go-hep](https://go-hep.org)<br/>
  Libraries and applications allowing High Energy Physicists to write efficient
  analysis code