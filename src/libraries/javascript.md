## JavaScript

*Libraries and frameworks for JavaScript and its ecosystem*

- [date-fns](https://date-fns.org)<br/>
  Modern JavaScript date utility library
  
- [ApexCharts](https://apexcharts.com)<br/>
  Beautiful JavaScript charts
  
- [immer](https://github.com/mweststrate/immer)<br/>
  Create the next immutable state by mutating the current one
