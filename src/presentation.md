# Presentation

*Creating compelling presentations without pixel-twiddling*

- [jxnblk/mdx-deck](https://github.com/jxnblk/mdx-deck)<br/>
  MDX-based presentation decks

- [RevealJS](https://revealjs.com/)<br/>
  The HTML presentation framework

- [evilz/vscode-reveal](https://github.com/evilz/vscode-reveal)<br/>
  RevealJS VSCode extension

- [ivanceras/svgbob](https://github.com/ivanceras/svgbob)<br/>
  Convert your ascii diagram scribbles into happy little SVG

- [rust-lang/mdBook](https://github.com/rust-lang/mdBook)<br/>
  A utility to create modern online books from Markdown files
