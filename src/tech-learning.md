# Tech Learning

*Plenty of articles, books and video for increasing technical know-how*

- [Packt Publishing Free Learning](https://www.packtpub.com/packt/offers/free-learning)<br/>
  Free programming e-books
  
- [Mapt](https://www.packtpub.com/mapt/)<br/>
  My library of e-books from Packt Publishing
  
- [learnbyexample/Command-line-text-processing](https://github.com/learnbyexample/Command-line-text-processing)<br/>
  From finding text to search and replace, from sorting to beautifying text

- [Exploring ES6](http://exploringjs.com/es6/)<br/>
  In-depth look at ECMAScript 6 by Axel Rauschmayer

- [Front-End Developer Handbook 2018](https://frontendmasters.com/books/front-end-handbook/2018/)<br/>
  This is a guide that anyone could use to learn about the practice of front-end
  development
  
- [Rico's Cheatsheets](https://devhints.io)<br/>
  A modest collection of cheatsheets by [@rstacruz](https://ricostacruz.com)

- [Tensor Programming](https://www.youtube.com/channel/UCYqCZOwHbnPwyjawKfE21wg/playlists)<br/>
  Video tutorials for a variety of programming languages and environments

- [Clean Architecture for Android with Kotlin](https://antonioleiva.com/clean-architecture-android/)<br/>
  From the blog of Antonio Leiva
