## Dotfiles

*Saving, applying and propagating common settings*

- [mathiasbynens](https://github.com/mathiasbynens/dotfiles)<br/>
  Godfather of dotfiles

- [mixn](https://github.com/mixn/dotfiles)<br/>
  macOS settings and links to other great dotfiles

- [vith](https://gitlab.com/vith/dotfiles)<br/>
  Custom, simplified stow targets