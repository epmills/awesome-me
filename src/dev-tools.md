# Development Tools

*Great tools for application developers*

- [Android Asset Studio](https://romannurik.github.io/AndroidAssetStudio/)<br/>
  Easily generate Android app assets

- [fastlane](https://fastlane.tools)<br/>
  The easiest way to automate beta deployments and releases for your iOS and
  Android apps

- [favicon.io](https://favicon.io)<br/>
  The ultimate favicon generator

- [Flipt](https://flipt.dev)<br/>
  A feature flag solution that runs in your existing infrastructure

- [GoogleChromeLabs/squoosh](https://squoosh.app)<br/>
  Make images smaller using best-in-class codecs, right in the browser

- [JSON-to-Go](https://mholt.github.io/json-to-go/)<br/>
  Convert JSON to Go struct

- [mozilla/sops](https://github.com/mozilla/sops)<br/>
  Encrypt YAML, JSON and binary files with PGP and KMS

- [priv.sh](https://priv.sh)<br/>
  A private link-shortening service that doesn’t track the people who click
  through it