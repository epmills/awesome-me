## Security

*Encryption, security and locking things down*

- [How To Secure A Linux Server](https://github.com/imthenachoman/How-To-Secure-A-Linux-Server)<br/>
 An evolving how-to guide for securing a Linux server 

- [skeeto/enchive](https://github.com/skeeto/enchive)<br/>
  Encrypted personal archives
