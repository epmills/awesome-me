## Network

*VPNs and alternative networking*

- [ZeroTier](https://www.zerotier.com)<br/> Every Area Networking

- [trailofbits/algo](https://github.com/trailofbits/algo)<br/>
  Set up a personal IPSEC VPN in the cloud

- [pirate/wireguard-docs](https://docs.sweeting.me/s/wireguard)<br/>
  The unofficial Wireguard documentation

