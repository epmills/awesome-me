# DevOps

*Keeping the machinery up and running*

- [Ansible](ansible.md)
- [Backups](backups.md)
- [Cloud](cloud.md)
- [Docker](docker.md)
- [Email](email.md)
- [Filesystems](filesystems.md)
- [Network](network.md)
- [Security](security.md)
- [Text Processing](text-processing.md)
- [Version Control](version-control.md)
- [Workflow and Automation](wf-automation.md)
