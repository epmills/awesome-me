## Email

*Running your own email server*

- [mailcow/mailcow-dockerized](https://github.com/mailcow/mailcow-dockerized/)<br/>
  mailcow is a mail server suite based on Dovecot, Postfix and other open source
  software

- [SOGo](https://sogo.nu)<br/>
  SOGo is a fully supported and trusted groupware server with a focus on
  scalability and open standards
  
- [tomav/docker-mailserver](https://tvi.al/simple-mail-server-with-docker/)<br/>
  A fullstack but simple mailserver (smtp, imap, antispam, antivirus, ssl...)
  using Docker
