## Workflow & Automation

*Let the computer do it!*

- [Jeffail/benthos](https://github.com/Jeffail/benthos)<br/>
  A dull, resilient and quick to deploy stream processor

- [liftbridge-io/liftbridge](https://github.com/liftbridge-io/liftbridge)<br/>
  Lightweight, fault-tolerant message streams

- [OVH µTask](https://github.com/ovh/utask)<br/>
  Automation engine that models and executes business processes
