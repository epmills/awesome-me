## Version Control

*Whoops!  How do I undo that, again?*

- [troyfontaine/1-setup.md](https://gist.github.com/troyfontaine/18c9146295168ee9ca2b30c00bd1b41e)<br/>
  Signing your Git Commits using GPG on MacOS Sierra/High Sierra
