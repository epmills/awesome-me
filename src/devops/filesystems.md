## Filesystems

*Alternative filesystems*

- [9fans/plan9port](https://9fans.github.io/plan9port/)<br/>
  Plan 9 from User Space

- [aqwari.net/9p](https://blog.aqwari.net/9p/)<br/>
  Writing a 9P server from scratch

- [A Beginner's Guide To btrfs](https://www.howtoforge.com/a-beginners-guide-to-btrfs)<br/>
  How to work with the btrfs file system on Linux
  