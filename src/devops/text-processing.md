## Text Processing

*Manipulating those lines, words and chars*

- [csvq](https://mithrandie.github.io/csvq/)<br/>
  SQL-like query language for CSV

- [espanso](https://espanso.org)<br/>
  Cross-platform text expander written in Rust

- [svenfuchs/jq](https://github.com/svenfuchs/jq)<br/>
  A high performance Golang implementation of the incredibly useful jq command line tool

- [peco/peco](https://github.com/peco/peco)<br/>
  Simplistic interactive filtering tool

- [hhrutter/pdfcpu](https://github.com/hhrutter/pdfcpu)<br/>
  pdfcpu: a golang pdf processor
