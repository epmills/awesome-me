## Ansible

*Tools related to Ansible, the simple IT automation engine*

- [Ansible Vault](http://docs.ansible.com/ansible/latest/user_guide/vault.html#)<br/>
  Allows you to keep sensitive data such as passwords or keys in encrypted files
