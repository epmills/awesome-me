## Backups

*Tools for saving your bacon*

- [Rclone](https://rclone.org)<br/>
  Command line program to sync files and directories
  
- [Duplicati 2.0](https://www.duplicati.com)<br/>
  Free backup software to store encrypted backups online
  
- [B2 with Restic](https://help.backblaze.com/hc/en-us/articles/115002880514-How-to-configure-Backblaze-B2-with-Restic-on-Linux)<br/>
  How to configure Backblaze B2 with Restic on Linux

- [billw2/rpi-clone](https://github.com/billw2/rpi-clone)<br/>
  A shell script to clone a booted disk.
