## Cloud

*Tools and services for running in the cloud*

- [LocalStack](https://localstack.cloud)<br/>
  Develop and test your cloud apps offline

- [Zeit](https://github.com/zeit)<br/>
  A global deployment network built on top of all existing cloud providers
